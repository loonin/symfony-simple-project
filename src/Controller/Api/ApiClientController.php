<?php

namespace App\Controller\Api;

use App\Entity\Client;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;


use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * @Route("/client")
 */
class ApiClientController extends AbstractController
{
    /**
     * @Route("/update", name="api_client_update",  methods={"POST"})
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function update(Request $request)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $validator = Validation::createValidator();
        $constraint = new Assert\Collection([
            'id' => new Assert\Optional(),
            'name' => new Assert\Length(['min' => 1]),
            'phone' => new Assert\Length(['min' => 1]),
            'bonuses' => new Assert\Length(['min' => 1]),
            'email' => new Assert\Email(),
            'enable' => new Assert\Choice([0, 1]),
            'address' => new Assert\Optional(),
        ]);
        $violations = $validator->validate($data, $constraint);

        if ($violations->count() > 0) {
            return new JsonResponse(["error" => (string)$violations], 500);
        }

        $entityManager = $this->getDoctrine()->getManager();

        if (!empty($data['id'])) {
            $client = $entityManager
                ->getRepository(Client::class)
                ->find($data['id']);
        }

        if (!isset($client) || $client === null) {
            $client = new Client();
        }

        $name = $data['name'];
        $phone = $data['phone'];
        $bonuses = $data['bonuses'];
        $email = $data['email'];

        $client
            ->setName($name)
            ->setPhone($phone)
            ->setEmail($email)
            ->setBonuses($bonuses)
            ->setEnabled(true)
        ;

        if (!empty($data['address'])) {
            $client->setAddress($data['address']);
        }

        $entityManager->persist($client);

        $entityManager->flush();

        return new JsonResponse(["success" => $client->toArray()], 200);
    }
}